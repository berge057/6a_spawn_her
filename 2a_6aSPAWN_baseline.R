# script that derives the HERAS index directly from the StoX projects.

rm(list=ls())

library(Rstox)
library(FLFleet)
library(FLCore)
library(ggplotFL)
library(mgcv)
library(tidyverse)
library(stats)
library(XML)
library(tidyr)

#Set up directories

#path <- 'J:/git/HERAS/'
#path <- 'J:/git/6a_spawn_her'
#try(setwd(path),silent=TRUE)

mainPath      <- file.path(".")
dataPath      <- file.path(".","data")
stoxPath      <- file.path(".","StoX")
outPath       <- file.path(".","output")
functionPath  <- file.path(".","functions")
resultsPath   <- file.path(".","results")

runName <- 'VIaSPAWN_baseline'

######################################################################
######################################################################
######################################################################
#### Define parameters and load functions and data
######################################################################
######################################################################
######################################################################

source(file.path(functionPath,"build_stox_project_tree.R"))
source(file.path(functionPath,"compute_6aSPAWN_strata_mat.R"))
source(file.path(functionPath,"fill_missing_all.R"))
source(file.path(functionPath,"fill_missing_mat_scale.R"))

project_mapping <- read.csv(file.path(dataPath,"2_project_mapping_baseline.csv"))

surveyYearMat    <- unique(project_mapping$year)

# parameters HERAS index
minYear <- 2016
maxYear <- 2022
yearVec <- minYear:maxYear
nYears  <- length(yearVec)
minAge  <- 0
maxAge  <- 9
ageVec  <- minAge:maxAge
nAges   <- length(ageVec)
nIter   <- 1
strataAll  <- c('Strata1','Strata2')
nStrata <- length(strataAll)

# VB parameters for age filling
VB_params <- as.data.frame(cbind(29.4106,0.4273759,-1.43335))
colnames(VB_params) <- c('Linf','K','t0')
components <- unique(project_mapping$component)
nComp <- length(components)

######################################################################
######################################################################
######################################################################
#### # initialise FLStock objects
######################################################################
######################################################################
######################################################################

# stock object
FLR_temp <- FLQuant( array(0,dim = c(nAges,nYears,nComp,1,1,nIter)),
                     dimnames=list( age=as.character(ageVec),
                                    year=as.character(yearVec),
                                    unit=components,
                                    season='all',
                                    area='all'),
                            units='1e6')

VIaSPAWN.stock <- FLStock(name = 'VIaSPAWN', 
                    desc = 'VIaSPAWN',
                    FLR_temp)

######################################################################
######################################################################
######################################################################
#### # initialise FLIndex objects
######################################################################
######################################################################
######################################################################

# EU NO index object
FLQ_HERAS_strata <- FLQuant(  array(0,dim = c(nAges,nYears,nComp,4,nStrata,nIter)),
                              dimnames=list(age=as.character(ageVec),
                                            year=as.character(yearVec),
                                            unit=components,
                                            season=c('all','IMM','MAT','SPAWN'),
                                            area=ac(strataAll)),
                              units='1e6')
FLI_VIaSPAWN_strata <- FLIndex( name = 'VIaSPAWN_strata', 
                                desc = 'VIaSPAWN index disagregated by strata', 
                                index=FLQ_HERAS_strata)

# total index object
FLQ_VIaSPAWN_all <- FLQuant(array(0,dim = c(nAges,nYears,nComp,4,1,nIter)),
                            dimnames=list(age=as.character(ageVec),
                                          year=as.character(yearVec),
                                          unit=components,
                                          season=c('all','IMM','MAT','SPAWN'),
                                          area='all'),
                            units='1e6')

FLI_VIaSPAWN_all <- FLIndex(name = 'VIaSPAWN_all',
                            desc = 'VIaSPAWN index (total)',
                            index=FLQ_VIaSPAWN_all)

VIaSPAWN.ind <- FLIndices(FLI_VIaSPAWN_strata,
                          FLI_VIaSPAWN_all)

######################################################################
######################################################################
######################################################################
#### Compute NSAS/WBSS indices
######################################################################
######################################################################
######################################################################

dir.create("StoX",showWarnings = FALSE)

setwd('StoX')

for(idxProject in 1:dim(project_mapping)[1]){
  surveyYear  <- project_mapping$year[idxProject]
  surveyComp  <- as.character(project_mapping$component[idxProject])
  
  print(project_mapping$project_main[idxProject])
  
  dir.create(as.character(surveyYear),showWarnings = FALSE)
  setwd(as.character(surveyYear))
  
  #################################################################
  ## run StoX projects
  #################################################################

  # build project
  build_stox_project_tree(project_mapping$project_main[idxProject],
                          surveyYear)
  
  currentProjects <- project_mapping$project_main[idxProject]
  #idxYear <- 2021
  
  projectPath <- file.path(getwd(),
                           project_mapping$project_main[idxProject])
  
  runBaseline(projectName = projectPath,
              save = TRUE,
              exportCSV = TRUE,
              modelType = c('baseline'))
  runBaseline(projectName = projectPath,
              save = TRUE,
              exportCSV = TRUE,
              modelType = c('baseline-report'))
  currentBaseLineReport <- getBaseline(projectPath,
                                       save = FALSE,
                                       exportCSV = FALSE,
                                       modelType = c('baseline-report'),reset = FALSE)
  
  endTab                <- currentBaseLineReport$outputData$FillMissingData
  endTab[is.na(endTab)] <- '-' # replace NA with '-'
  endTab[endTab == "NA"] <- '-' # replace NA with '-'

  #################################################################
  ## create index and assign values
  #################################################################
  
  # rename strata
  if(project_mapping$rename_strata[idxProject] == 1){
    endTab$Stratum[endTab$Stratum == '1A' | endTab$Stratum == '1B'] <- 'Strata1'
    endTab$Stratum[endTab$Stratum == '2A' | endTab$Stratum == '2B'] <- 'Strata2'
  }
  
  # fill in missing cells
  if(project_mapping$fill_cells[idxProject] == 'stock_mat'){
    out <- fill_missing_all(endTab,mode='Abundance',VB_params,maxAge)
  }else if(project_mapping$fill_cells[idxProject] == 'mat'){
    out <- fill_missing_mat_scale(endTab,mode='Abundance',VB_params,maxAge)
  }
  
  # write tables
  write.csv(x = out$endTab,
            file = file.path('../..',resultsPath,surveyYear,
                             paste0(surveyYear,'_',
                                    runName,
                                    '_',
                                    surveyComp,
                                    '_filled.csv')))
  
  write.csv(x = out$endTabInit,
            file = file.path('../..',resultsPath,surveyYear,
                             paste0(surveyYear,'_',
                                    runName,
                                    '_',
                                    surveyComp,
                                    '_init.csv')),
            row.names = F)
  
  # allocate MAT/IMM
  if(project_mapping$mat_scale[idxProject]){
    out$endTab$maturity_scale <- out$endTab$maturity
    out$endTab$maturity[out$endTab$maturity_scale <= 2] <- 'IMM'
    out$endTab$maturity[out$endTab$maturity_scale >= 2 & out$endTab$maturity_scale <= 5] <- 'MAT'
    out$endTab$maturity[out$endTab$maturity_scale == 6] <- 'ABN'
  }
  
  endTab <- out$endTab
  spawn_mode <- project_mapping$spawn_mode[idxProject]
  
  outIndex <- compute_6aSPAWN_strata_mat(out$endTab,
                                         ageVec,
                                         surveyYear,
                                         strataAll,
                                         project_mapping$spawn_mode[idxProject])
  
  for(idxStrata in strataAll){
    for(idxMAT in c('all','IMM','MAT','SPAWN')){
      VIaSPAWN.ind$VIaSPAWN_strata@index[,ac(surveyYear),
                                         surveyComp,
                                         idxMAT,ac(idxStrata)] <- 
            outIndex$VIaSPAWN[ac(idxStrata),,idxMAT,'abu']*1e-6
      VIaSPAWN.ind$VIaSPAWN_strata@index.q[,ac(surveyYear),
                                           surveyComp,
                                           idxMAT,ac(idxStrata)] <- 
            outIndex$VIaSPAWN[ac(idxStrata),,idxMAT,'biomass']*1e-9
    }
  }
  
  # fill in stock weight at age and maturity
  # stock weight
  VIaSPAWN.stock@stock.wt[,ac(surveyYear),
                          surveyComp]  <- outIndex$wa$VIaSPAWN
  #VIaSPAWN.stock@stock.wt[is.na(VIaSPAWN.stock@stock.wt)] <- 0
  
  # small hack here, storing length at age in catch weight field
  VIaSPAWN.stock@catch.wt[,ac(surveyYear),
                          surveyComp]  <- outIndex$la$VIaSPAWN 
  #VIaSPAWN.stock@catch.wt[is.na(VIaSPAWN.stock@stock.wt)] <- 0
  
  # maturity at age
  VIaSPAWN.stock@mat[,ac(surveyYear),
                     surveyComp]       <- outIndex$propM$VIaSPAWN
  #VIaSPAWN.stock@mat[is.na(VIaSPAWN.stock@mat)] <- 0
  
  VIaSPAWN.stock@stock.n[,ac(surveyYear),
                         surveyComp]   <- areaSums(VIaSPAWN.ind$VIaSPAWN_strata@index[,ac(surveyYear),
                                                                                      surveyComp,
                                                                                      'all'])
  #VIaSPAWN.stock@stock.n[is.na(VIaSPAWN.stock@stock.n)] <- 0

  # fill in SSB
  VIaSPAWN.stock@stock[,ac(surveyYear),
                       surveyComp] <- sum( VIaSPAWN.stock@stock.n[,ac(surveyYear),
                                                                  surveyComp]*
                                                VIaSPAWN.stock@stock.wt[,ac(surveyYear),
                                                                        surveyComp]*
                                                VIaSPAWN.stock@mat[,ac(surveyYear),
                                                                   surveyComp])
  
  VIaSPAWN.stock <- qapply(VIaSPAWN.stock, function(x){
                      x[is.na(x)] <- 0 
                      return(x)})

  setwd('..')
}

setwd('..')

# combine all strata
VIaSPAWN.ind$VIaSPAWN_all@index[]   <- areaSums(VIaSPAWN.ind$VIaSPAWN_strata@index)
VIaSPAWN.ind$VIaSPAWN_all@index.q[] <- areaSums(VIaSPAWN.ind$VIaSPAWN_strata@index.q)

VIaSPAWN_baseline <- list(VIaSPAWN.ind=VIaSPAWN.ind,
                          VIaSPAWN.stock=VIaSPAWN.stock)

save(VIaSPAWN_baseline,
     file = file.path(resultsPath,paste0(runName,'.RData')))

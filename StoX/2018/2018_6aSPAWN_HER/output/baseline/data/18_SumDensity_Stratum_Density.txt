SpecCat	SampleUnitType	SampleUnit	SampleSize	PosSampleSize	Distance	LayerType	Layer	EstLayer	EstLayerDef	LengthGroup	LengthInterval	Density
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	8.5	0.5	1168.274
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	9.0	0.5	1168.274
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	9.5	0.5	7009.647
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	10.0	0.5	15187.568
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	10.5	0.5	12851.019
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	11.0	0.5	46730.977
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	11.5	0.5	26870.312
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	12.0	0.5	7009.647
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	12.5	0.5	1168.274
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	13.0	0.5	1168.274
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	19.0	0.5	461.335
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	19.5	0.5	442.825
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	20.0	0.5	1309.965
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	20.5	0.5	424.315
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	21.0	0.5	636.472
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	22.0	0.5	212.157
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	23.0	0.5	8063.911
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	23.5	0.5	12504.958
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	24.0	0.5	13198.895
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	24.5	0.5	7140.854
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	25.0	0.5	16475.354
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	25.5	0.5	19824.465
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	26.0	0.5	29410.111
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	26.5	0.5	24278.608
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	27.0	0.5	35769.346
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	27.5	0.5	37397.274
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	28.0	0.5	25497.445
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	28.5	0.5	34617.590
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	29.0	0.5	22591.317
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	29.5	0.5	13872.423
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	30.0	0.5	9604.157
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	30.5	0.5	3588.937
Clupea harengus	Stratum	Strata1	191	-	187.100	-	-	1	PELBOT	31.0	0.5	1272.557
Clupea harengus	Stratum	Strata2	175	-	173.400	-	-	1	PELBOT	22.5	0.5	1307.405
Clupea harengus	Stratum	Strata2	175	-	173.400	-	-	1	PELBOT	23.0	0.5	1307.405
Clupea harengus	Stratum	Strata2	175	-	173.400	-	-	1	PELBOT	23.5	0.5	6537.023
Clupea harengus	Stratum	Strata2	175	-	173.400	-	-	1	PELBOT	24.0	0.5	9151.833
Clupea harengus	Stratum	Strata2	175	-	173.400	-	-	1	PELBOT	24.5	0.5	1307.405
Clupea harengus	Stratum	Strata2	175	-	173.400	-	-	1	PELBOT	25.0	0.5	7844.428
Clupea harengus	Stratum	Strata2	175	-	173.400	-	-	1	PELBOT	25.5	0.5	6537.023
Clupea harengus	Stratum	Strata2	175	-	173.400	-	-	1	PELBOT	26.0	0.5	7844.428
Clupea harengus	Stratum	Strata2	175	-	173.400	-	-	1	PELBOT	26.5	0.5	10459.237
Clupea harengus	Stratum	Strata2	175	-	173.400	-	-	1	PELBOT	27.0	0.5	9151.833
Clupea harengus	Stratum	Strata2	175	-	173.400	-	-	1	PELBOT	27.5	0.5	16996.261
Clupea harengus	Stratum	Strata2	175	-	173.400	-	-	1	PELBOT	28.0	0.5	16996.261
Clupea harengus	Stratum	Strata2	175	-	173.400	-	-	1	PELBOT	28.5	0.5	5229.619
Clupea harengus	Stratum	Strata2	175	-	173.400	-	-	1	PELBOT	29.0	0.5	10459.237
Clupea harengus	Stratum	Strata2	175	-	173.400	-	-	1	PELBOT	29.5	0.5	9151.833
Clupea harengus	Stratum	Strata2	175	-	173.400	-	-	1	PELBOT	30.0	0.5	5229.619
Clupea harengus	Stratum	Strata2	175	-	173.400	-	-	1	PELBOT	30.5	0.5	3922.214
Clupea harengus	Stratum	Strata2	175	-	173.400	-	-	1	PELBOT	31.0	0.5	1307.405

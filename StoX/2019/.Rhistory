setwd(as.character(surveyYear))
#################################################################
## run StoX projects
#################################################################
# build project
build_stox_project_tree(project_mapping$project_main[idxProject],
surveyYear)
projectPath <- file.path(getwd(),
project_mapping$project_main[idxProject])
runBaseline(projectName = projectPath,
save = TRUE,
exportCSV = TRUE,
modelType = c('baseline'))
runBaseline(projectName = projectPath,
save = TRUE,
exportCSV = TRUE,
modelType = c('baseline-report'))
runBootstrap(projectName=projectPath,
bootstrapMethod="AcousticTrawl",
acousticMethod="PSU~Stratum",
bioticMethod="PSU~Stratum",
startProcess="TotalLengthDist",
endProcess="SuperIndAbundance",
nboot=nIter,
seed=1, cores=4)
saveProjectData(projectPath)
# load bootstrap objects
load(file.path(projectPath,'output','r','data','bootstrap.RData'))
bootstrapU <- bootstrap
# loop on bootstrap objects
for(idxIter in 1:nIter){
print(idxIter/nIter)
#################################################################
## create index and assign values
#################################################################
SuperIndAbundance      <- bootstrap$SuperIndAbundance[idxIter][[1]]
endTab                  <- distributeAbundance(abnd=SuperIndAbundance)$data
endTab[is.na(endTab)] <- '-' # replace NA with '-'
endTab[endTab == "NA"] <- '-' # replace NA with '-'
# some iterations have no weights therefore filtering out these
if(any(endTab$weight != '-' & endTab$length != '-')){
#which(endTab$weight != '-' & endTab$length != '-')
#cbind(endTab$weight,endTab$length)
#out <- fill_missing_all(endTab,mode='Abundance',VB_params,maxAge)
# rename strata
if(project_mapping$rename_strata[idxProject] == 1){
endTab$Stratum[endTab$Stratum == '1A' | endTab$Stratum == '1B'] <- 'Strata1'
endTab$Stratum[endTab$Stratum == '2A' | endTab$Stratum == '2B'] <- 'Strata2'
}
# fill in missing cells
if(project_mapping$fill_cells[idxProject] == 'stock_mat'){
out <- fill_missing_all(endTab,mode='Abundance',VB_params,maxAge)
}else if(project_mapping$fill_cells[idxProject] == 'mat'){
out <- fill_missing_mat_scale(endTab,mode='Abundance',VB_params,maxAge)
}
# allocate MAT/IMM
if(project_mapping$mat_scale[idxProject]){
out$endTab$maturity_scale <- out$endTab$maturity
out$endTab$maturity[out$endTab$maturity_scale <= 2] <- 'IMM'
out$endTab$maturity[out$endTab$maturity_scale >= 2 & out$endTab$maturity_scale <= 5] <- 'MAT'
out$endTab$maturity[out$endTab$maturity_scale == 6] <- 'ABN'
}
outIndex <- compute_6aSPAWN_strata_mat(out$endTab,
ageVec,
surveyYear,
strataAll,
project_mapping$spawn_mode[idxProject])
for(idxStrata in strataAll){
for(idxMAT in c('all','IMM','MAT','SPAWN')){
VIaSPAWN.ind$VIaSPAWN_strata@index[,ac(surveyYear),surveyComp,idxMAT,ac(idxStrata),idxIter] <-
outIndex$VIaSPAWN[ac(idxStrata),,idxMAT,'abu']*1e-6
VIaSPAWN.ind$VIaSPAWN_strata@index.q[,ac(surveyYear),surveyComp,idxMAT,ac(idxStrata),idxIter] <-
outIndex$VIaSPAWN[ac(idxStrata),,idxMAT,'biomass']*1e-9
}
}
# fill in stock weight at age and maturity
VIaSPAWN.stock@stock.wt[,ac(surveyYear),surveyComp,,,idxIter]  <- outIndex$wa$VIaSPAWN
VIaSPAWN.stock@catch.wt[,ac(surveyYear),surveyComp,,,idxIter]  <- outIndex$la$VIaSPAWN # hack, storing length at age in catch weight field
VIaSPAWN.stock@mat[,ac(surveyYear),surveyComp,,,idxIter]       <- outIndex$propM$VIaSPAWN
VIaSPAWN.stock@stock.n[,ac(surveyYear),surveyComp,,,idxIter]   <- areaSums(iter(VIaSPAWN.ind$VIaSPAWN_strata@index[,ac(surveyYear),surveyComp,'all'],idxIter))
# fill in SSB
VIaSPAWN.stock@stock[,ac(surveyYear),surveyComp,,,idxIter] <- sum(iter(VIaSPAWN.stock@stock.n[,ac(surveyYear),surveyComp],idxIter)*
iter(VIaSPAWN.stock@stock.wt[,ac(surveyYear),surveyComp],idxIter)*
iter(VIaSPAWN.stock@mat[,ac(surveyYear),surveyComp],idxIter))
}else{
idxFilt <- rbind(idxFilt,c(surveyYear,idxIter))
}
}
#VIaSPAWN.stock  <- iter(VIaSPAWN.stock,idxFilt)
#VIaSPAWN.ind    <- iter(VIaSPAWN.ind,idxFilt)
setwd('..')
}
setwd('..')
VIaSPAWN.ind    <- iter(VIaSPAWN.ind,which(!(seq(1:nIter) %in% idxFilt)))
VIaSPAWN.stock  <- iter(VIaSPAWN.stock,which(!(seq(1:nIter) %in% idxFilt)))
VIaSPAWN.ind$VIaSPAWN_all@index[] <- areaSums(VIaSPAWN.ind$VIaSPAWN_strata@index)
VIaSPAWN.ind$VIaSPAWN_all@index.q[] <- areaSums(VIaSPAWN.ind$VIaSPAWN_strata@index.q)
VIaSPAWN_bootstrap <- list( VIaSPAWN.ind=VIaSPAWN.ind,
VIaSPAWN.stock=VIaSPAWN.stock)
save(VIaSPAWN_bootstrap,
file = file.path(resultsPath,paste0(runName,'.RData')))
# script that derives the HERAS index directly from the StoX projects.
rm(list=ls())
library(Rstox)
library(FLFleet)
library(FLCore)
library(ggplotFL)
library(mgcv)
library(tidyverse)
library(stats)
library(XML)
library(tidyr)
#Set up directories
#path <- 'J:/git/HERAS/'
#path <- 'J:/git/6a_spawn_her'
#try(setwd(path),silent=TRUE)
mainPath      <- file.path(".")
dataPath      <- file.path(".","data")
stoxPath      <- file.path(".","StoX")
outPath       <- file.path(".","output")
functionPath  <- file.path(".","functions")
resultsPath   <- file.path(".","results")
runName <- 'VIaSPAWN_baseline'
######################################################################
######################################################################
######################################################################
#### Define parameters and load functions and data
######################################################################
######################################################################
######################################################################
source(file.path(functionPath,"build_stox_project_tree.R"))
source(file.path(functionPath,"compute_6aSPAWN_strata_mat.R"))
source(file.path(functionPath,"fill_missing_all.R"))
source(file.path(functionPath,"fill_missing_mat_scale.R"))
project_mapping <- read.csv(file.path(dataPath,"2_project_mapping_baseline.csv"))
surveyYearMat    <- unique(project_mapping$year)
# parameters HERAS index
minYear <- 2016
maxYear <- 2021
yearVec <- minYear:maxYear
nYears  <- length(yearVec)
minAge  <- 0
maxAge  <- 9
ageVec  <- minAge:maxAge
nAges   <- length(ageVec)
nIter   <- 1
strataAll  <- c('Strata1','Strata2')
nStrata <- length(strataAll)
# VB parameters for age filling
VB_params <- as.data.frame(cbind(29.4106,0.4273759,-1.43335))
colnames(VB_params) <- c('Linf','K','t0')
components <- unique(project_mapping$component)
nComp <- length(components)
######################################################################
######################################################################
######################################################################
#### # initialise FLStock objects
######################################################################
######################################################################
######################################################################
# stock object
FLR_temp <- FLQuant( array(0,dim = c(nAges,nYears,nComp,1,1,nIter)),
dimnames=list( age=as.character(ageVec),
year=as.character(yearVec),
unit=components,
season='all',
area='all'),
units='1e6')
VIaSPAWN.stock <- FLStock(name = 'VIaSPAWN',
desc = 'VIaSPAWN',
FLR_temp)
######################################################################
######################################################################
######################################################################
#### # initialise FLIndex objects
######################################################################
######################################################################
######################################################################
# EU NO index object
FLQ_HERAS_strata <- FLQuant(  array(0,dim = c(nAges,nYears,nComp,4,nStrata,nIter)),
dimnames=list(age=as.character(ageVec),
year=as.character(yearVec),
unit=components,
season=c('all','IMM','MAT','SPAWN'),
area=ac(strataAll)),
units='1e6')
FLI_VIaSPAWN_strata <- FLIndex( name = 'VIaSPAWN_strata',
desc = 'VIaSPAWN index disagregated by strata',
index=FLQ_HERAS_strata)
# total index object
FLQ_VIaSPAWN_all <- FLQuant(array(0,dim = c(nAges,nYears,nComp,4,1,nIter)),
dimnames=list(age=as.character(ageVec),
year=as.character(yearVec),
unit=components,
season=c('all','IMM','MAT','SPAWN'),
area='all'),
units='1e6')
FLI_VIaSPAWN_all <- FLIndex(name = 'VIaSPAWN_all',
desc = 'VIaSPAWN index (total)',
index=FLQ_VIaSPAWN_all)
VIaSPAWN.ind <- FLIndices(FLI_VIaSPAWN_strata,
FLI_VIaSPAWN_all)
######################################################################
######################################################################
######################################################################
#### Compute NSAS/WBSS indices
######################################################################
######################################################################
######################################################################
dir.create("StoX",showWarnings = FALSE)
setwd('StoX')
for(idxProject in 1:dim(project_mapping
project_mapping
# script that derives the HERAS index directly from the StoX projects.
rm(list=ls())
library(Rstox)
library(FLFleet)
library(FLCore)
library(ggplotFL)
library(mgcv)
library(tidyverse)
library(stats)
library(XML)
library(tidyr)
#Set up directories
#path <- 'J:/git/HERAS/'
#path <- 'J:/git/6a_spawn_her'
#try(setwd(path),silent=TRUE)
mainPath      <- file.path(".")
dataPath      <- file.path(".","data")
stoxPath      <- file.path(".","StoX")
outPath       <- file.path(".","output")
functionPath  <- file.path(".","functions")
resultsPath   <- file.path(".","results")
runName <- 'VIaSPAWN_baseline'
######################################################################
######################################################################
######################################################################
#### Define parameters and load functions and data
######################################################################
######################################################################
######################################################################
source(file.path(functionPath,"build_stox_project_tree.R"))
source(file.path(functionPath,"compute_6aSPAWN_strata_mat.R"))
source(file.path(functionPath,"fill_missing_all.R"))
source(file.path(functionPath,"fill_missing_mat_scale.R"))
project_mapping <- read.csv(file.path(dataPath,"2_project_mapping_baseline.csv"))
surveyYearMat    <- unique(project_mapping$year)
# parameters HERAS index
minYear <- 2016
maxYear <- 2021
yearVec <- minYear:maxYear
nYears  <- length(yearVec)
minAge  <- 0
maxAge  <- 9
ageVec  <- minAge:maxAge
nAges   <- length(ageVec)
nIter   <- 1
strataAll  <- c('Strata1','Strata2')
nStrata <- length(strataAll)
# VB parameters for age filling
VB_params <- as.data.frame(cbind(29.4106,0.4273759,-1.43335))
colnames(VB_params) <- c('Linf','K','t0')
components <- unique(project_mapping$component)
nComp <- length(components)
######################################################################
######################################################################
######################################################################
#### # initialise FLStock objects
######################################################################
######################################################################
######################################################################
# stock object
FLR_temp <- FLQuant( array(0,dim = c(nAges,nYears,nComp,1,1,nIter)),
dimnames=list( age=as.character(ageVec),
year=as.character(yearVec),
unit=components,
season='all',
area='all'),
units='1e6')
VIaSPAWN.stock <- FLStock(name = 'VIaSPAWN',
desc = 'VIaSPAWN',
FLR_temp)
######################################################################
######################################################################
######################################################################
#### # initialise FLIndex objects
######################################################################
######################################################################
######################################################################
# EU NO index object
FLQ_HERAS_strata <- FLQuant(  array(0,dim = c(nAges,nYears,nComp,4,nStrata,nIter)),
dimnames=list(age=as.character(ageVec),
year=as.character(yearVec),
unit=components,
season=c('all','IMM','MAT','SPAWN'),
area=ac(strataAll)),
units='1e6')
FLI_VIaSPAWN_strata <- FLIndex( name = 'VIaSPAWN_strata',
desc = 'VIaSPAWN index disagregated by strata',
index=FLQ_HERAS_strata)
# total index object
FLQ_VIaSPAWN_all <- FLQuant(array(0,dim = c(nAges,nYears,nComp,4,1,nIter)),
dimnames=list(age=as.character(ageVec),
year=as.character(yearVec),
unit=components,
season=c('all','IMM','MAT','SPAWN'),
area='all'),
units='1e6')
FLI_VIaSPAWN_all <- FLIndex(name = 'VIaSPAWN_all',
desc = 'VIaSPAWN index (total)',
index=FLQ_VIaSPAWN_all)
VIaSPAWN.ind <- FLIndices(FLI_VIaSPAWN_strata,
FLI_VIaSPAWN_all)
######################################################################
######################################################################
######################################################################
#### Compute NSAS/WBSS indices
######################################################################
######################################################################
######################################################################
dir.create("StoX",showWarnings = FALSE)
setwd('StoX')
project_mapping
# script that derives the HERAS index directly from the StoX projects.
rm(list=ls())
library(Rstox)
library(FLFleet)
library(FLCore)
library(ggplotFL)
library(mgcv)
library(tidyverse)
library(stats)
library(XML)
library(tidyr)
#Set up directories
#path <- 'J:/git/HERAS/'
#path <- 'J:/git/6a_spawn_her'
#try(setwd(path),silent=TRUE)
mainPath      <- file.path(".")
dataPath      <- file.path(".","data")
stoxPath      <- file.path(".","StoX")
outPath       <- file.path(".","output")
functionPath  <- file.path(".","functions")
resultsPath   <- file.path(".","results")
runName <- 'VIaSPAWN_baseline'
######################################################################
######################################################################
######################################################################
#### Define parameters and load functions and data
######################################################################
######################################################################
######################################################################
source(file.path(functionPath,"build_stox_project_tree.R"))
source(file.path(functionPath,"compute_6aSPAWN_strata_mat.R"))
source(file.path(functionPath,"fill_missing_all.R"))
source(file.path(functionPath,"fill_missing_mat_scale.R"))
project_mapping <- read.csv(file.path(dataPath,"2_project_mapping_baseline.csv"))
surveyYearMat    <- unique(project_mapping$year)
getwd()
setwd('E:\git\6a_spawn_her')
setwd('E:/git/6a_spawn_her')
# script that derives the HERAS index directly from the StoX projects.
rm(list=ls())
library(Rstox)
library(FLFleet)
library(FLCore)
library(ggplotFL)
library(mgcv)
library(tidyverse)
library(stats)
library(XML)
library(tidyr)
#Set up directories
#path <- 'J:/git/HERAS/'
#path <- 'J:/git/6a_spawn_her'
#try(setwd(path),silent=TRUE)
mainPath      <- file.path(".")
dataPath      <- file.path(".","data")
stoxPath      <- file.path(".","StoX")
outPath       <- file.path(".","output")
functionPath  <- file.path(".","functions")
resultsPath   <- file.path(".","results")
runName <- 'VIaSPAWN_baseline'
######################################################################
######################################################################
######################################################################
#### Define parameters and load functions and data
######################################################################
######################################################################
######################################################################
source(file.path(functionPath,"build_stox_project_tree.R"))
source(file.path(functionPath,"compute_6aSPAWN_strata_mat.R"))
source(file.path(functionPath,"fill_missing_all.R"))
source(file.path(functionPath,"fill_missing_mat_scale.R"))
project_mapping <- read.csv(file.path(dataPath,"2_project_mapping_baseline.csv"))
surveyYearMat    <- unique(project_mapping$year)
# parameters HERAS index
minYear <- 2016
maxYear <- 2021
yearVec <- minYear:maxYear
nYears  <- length(yearVec)
minAge  <- 0
maxAge  <- 9
ageVec  <- minAge:maxAge
nAges   <- length(ageVec)
nIter   <- 1
strataAll  <- c('Strata1','Strata2')
nStrata <- length(strataAll)
# VB parameters for age filling
VB_params <- as.data.frame(cbind(29.4106,0.4273759,-1.43335))
colnames(VB_params) <- c('Linf','K','t0')
components <- unique(project_mapping$component)
nComp <- length(components)
######################################################################
######################################################################
######################################################################
#### # initialise FLStock objects
######################################################################
######################################################################
######################################################################
# stock object
FLR_temp <- FLQuant( array(0,dim = c(nAges,nYears,nComp,1,1,nIter)),
dimnames=list( age=as.character(ageVec),
year=as.character(yearVec),
unit=components,
season='all',
area='all'),
units='1e6')
VIaSPAWN.stock <- FLStock(name = 'VIaSPAWN',
desc = 'VIaSPAWN',
FLR_temp)
######################################################################
######################################################################
######################################################################
#### # initialise FLIndex objects
######################################################################
######################################################################
######################################################################
# EU NO index object
FLQ_HERAS_strata <- FLQuant(  array(0,dim = c(nAges,nYears,nComp,4,nStrata,nIter)),
dimnames=list(age=as.character(ageVec),
year=as.character(yearVec),
unit=components,
season=c('all','IMM','MAT','SPAWN'),
area=ac(strataAll)),
units='1e6')
FLI_VIaSPAWN_strata <- FLIndex( name = 'VIaSPAWN_strata',
desc = 'VIaSPAWN index disagregated by strata',
index=FLQ_HERAS_strata)
# total index object
FLQ_VIaSPAWN_all <- FLQuant(array(0,dim = c(nAges,nYears,nComp,4,1,nIter)),
dimnames=list(age=as.character(ageVec),
year=as.character(yearVec),
unit=components,
season=c('all','IMM','MAT','SPAWN'),
area='all'),
units='1e6')
FLI_VIaSPAWN_all <- FLIndex(name = 'VIaSPAWN_all',
desc = 'VIaSPAWN index (total)',
index=FLQ_VIaSPAWN_all)
VIaSPAWN.ind <- FLIndices(FLI_VIaSPAWN_strata,
FLI_VIaSPAWN_all)
######################################################################
######################################################################
######################################################################
#### Compute NSAS/WBSS indices
######################################################################
######################################################################
######################################################################
dir.create("StoX",showWarnings = FALSE)
setwd('StoX')
project_mapping
idxProject<-5
surveyYear  <- project_mapping$year[idxProject]
surveyComp  <- as.character(project_mapping$component[idxProject])
print(project_mapping$project_main[idxProject])
dir.create(as.character(surveyYear),showWarnings = FALSE)
setwd(as.character(surveyYear))
# build project
build_stox_project_tree(project_mapping$project_main[idxProject],
surveyYear)
currentProjects <- project_mapping$project_main[idxProject]
idxYear <- 2021
projectPath <- file.path(getwd(),
project_mapping$project_main[idxProject])
runBaseline(projectName = projectPath,
save = TRUE,
exportCSV = TRUE,
modelType = c('baseline'))
runBaseline(projectName = projectPath,
save = TRUE,
exportCSV = TRUE,
modelType = c('baseline-report'))
currentBaseLineReport <- getBaseline(projectPath,
save = FALSE,
exportCSV = FALSE,
modelType = c('baseline-report'),reset = FALSE)
endTab                <- currentBaseLineReport$outputData$FillMissingData
endTab[is.na(endTab)] <- '-' # replace NA with '-'
endTab[endTab == "NA"] <- '-' # replace NA with '-'
# rename strata
if(project_mapping$rename_strata[idxProject] == 1){
endTab$Stratum[endTab$Stratum == '1A' | endTab$Stratum == '1B'] <- 'Strata1'
endTab$Stratum[endTab$Stratum == '2A' | endTab$Stratum == '2B'] <- 'Strata2'
}
# fill in missing cells
if(project_mapping$fill_cells[idxProject] == 'stock_mat'){
out <- fill_missing_all(endTab,mode='Abundance',VB_params,maxAge)
}else if(project_mapping$fill_cells[idxProject] == 'mat'){
out <- fill_missing_mat_scale(endTab,mode='Abundance',VB_params,maxAge)
}

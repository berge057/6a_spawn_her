missiontype	cruise	serialno	platform	startdate	station	fishstationtype	latitudestart	longitudestart	system	area	location	stratum	bottomdepthstart	bottomdepthstop	gear	gearcount	gearspeed	starttime	logstart	stoptime	distance	gearcondition	trawlquality	fishingdepthmax	fishingdepthmin	fishingdepthcount	trawlopening	trawldoorspread	latitudeend	longitudeend	wirelength	stopdate	logstop	flowcount	flowconst	comment
-	NL6aSPAWN2020	1	PBVO	-	1	-	58.9833	-7.1333	-	-	-	-	-	-	PEL	-	-	08:30:00	-	09:15:00	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-
-	NL6aSPAWN2020	2	PBVO	-	2	-	58.7183	-5.1467	-	-	-	-	-	-	PEL	-	-	16:48:00	-	17:25:00	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-
-	NL6aSPAWN2020	3	PBVO	-	3	-	58.6517	-5.6083	-	-	-	-	-	-	PEL	-	-	12:25:00	-	13:01:00	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-
-	NL6aSPAWN2020	4	PBVO	-	4	-	58.5600	-5.4383	-	-	-	-	-	-	PEL	-	-	18:26:00	-	19:03:00	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-
-	NL6aSPAWN2020	5	PBVO	-	5	-	58.5017	-5.5483	-	-	-	-	-	-	PEL	-	-	09:26:00	-	10:10:00	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-
-	OS0120	1	MXHR6	-	-	-	58.4333	-5.5833	-	-	-	-	-	-	PEL	-	-	08:35:00	-	09:04:00	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-
-	OS0120	2	MXHR6	-	-	-	58.5167	-5.6167	-	-	-	-	-	-	PEL	-	-	15:05:00	-	15:25:00	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-

#Trigger script automatically created by StoX at r
#Create time: 06/04/2022 13:53:29
#Load Rstox
library(Rstox)
runBootstrap.out <- runBootstrap(projectName="C:/git/6a_spawn_her/StoX/2021/2021_6aSPAWN_HER_NL", bootstrapMethod="AcousticTrawl", acousticMethod="PSU~Stratum", bioticMethod="PSU~Stratum", startProcess="TotalLengthDist", endProcess="SuperIndAbundance", nboot=1000, seed=1, cores=1)
imputeByAge.out <- imputeByAge(projectName="C:/git/6a_spawn_her/StoX/2021/2021_6aSPAWN_HER_NL", seed=1, cores=1)
saveProjectData.out <- saveProjectData(projectName="C:/git/6a_spawn_her/StoX/2021/2021_6aSPAWN_HER_NL")

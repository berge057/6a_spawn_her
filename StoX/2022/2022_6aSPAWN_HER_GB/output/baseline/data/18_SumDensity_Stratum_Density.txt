SpecCat	SampleUnitType	SampleUnit	SampleSize	PosSampleSize	Distance	LayerType	Layer	EstLayer	EstLayerDef	LengthGroup	LengthInterval	Density
Clupea harengus	Stratum	Strata1	403	-	403.000	-	-	1	PELBOT	24.5	0.5	76.214
Clupea harengus	Stratum	Strata1	403	-	403.000	-	-	1	PELBOT	25.5	0.5	152.427
Clupea harengus	Stratum	Strata1	403	-	403.000	-	-	1	PELBOT	26.0	0.5	609.709
Clupea harengus	Stratum	Strata1	403	-	403.000	-	-	1	PELBOT	26.5	0.5	914.563
Clupea harengus	Stratum	Strata1	403	-	403.000	-	-	1	PELBOT	27.0	0.5	762.136
Clupea harengus	Stratum	Strata1	403	-	403.000	-	-	1	PELBOT	27.5	0.5	1066.990
Clupea harengus	Stratum	Strata1	403	-	403.000	-	-	1	PELBOT	28.0	0.5	990.777
Clupea harengus	Stratum	Strata1	403	-	403.000	-	-	1	PELBOT	28.5	0.5	1143.204
Clupea harengus	Stratum	Strata1	403	-	403.000	-	-	1	PELBOT	29.0	0.5	838.350
Clupea harengus	Stratum	Strata1	403	-	403.000	-	-	1	PELBOT	29.5	0.5	457.282
Clupea harengus	Stratum	Strata1	403	-	403.000	-	-	1	PELBOT	30.0	0.5	381.068
Clupea harengus	Stratum	Strata1	403	-	403.000	-	-	1	PELBOT	30.5	0.5	76.214
Clupea harengus	Stratum	Strata1	403	-	403.000	-	-	1	PELBOT	31.0	0.5	76.214
Clupea harengus	Stratum	Strata1	403	-	403.000	-	-	1	PELBOT	32.5	0.5	76.214
Clupea harengus	Stratum	Strata2	181	-	181.000	-	-	1	PELBOT	10.0	0.5	0.000
Clupea harengus	Stratum	Strata2	181	-	181.000	-	-	1	PELBOT	10.5	0.5	0.000
Clupea harengus	Stratum	Strata2	181	-	181.000	-	-	1	PELBOT	11.0	0.5	0.000
Clupea harengus	Stratum	Strata2	181	-	181.000	-	-	1	PELBOT	11.5	0.5	0.000
Clupea harengus	Stratum	Strata2	181	-	181.000	-	-	1	PELBOT	12.0	0.5	0.000
Clupea harengus	Stratum	Strata2	181	-	181.000	-	-	1	PELBOT	12.5	0.5	0.000
Clupea harengus	Stratum	Strata2	181	-	181.000	-	-	1	PELBOT	13.0	0.5	0.000

SampleUnitType	SampleUnit	EstLayer	EstLayerDef	ObservationType	Observation	LengthInterval	Included
Stratum	Strata1	1	PELBOT	Station	CH0121/5	0.5	true
Stratum	Strata1	1	PELBOT	Station	CH0121/7	0.5	true
Stratum	Strata1	1	PELBOT	Station	CH0121/8	0.5	true
Stratum	Strata1	1	PELBOT	Station	CH0121/10	0.5	true
Stratum	Strata1	1	PELBOT	Station	CH0121/14	0.5	true
Stratum	Strata1	1	PELBOT	Station	CH0121/15	0.5	true
Stratum	Strata1	1	PELBOT	Station	CH0121/17	0.5	true
Stratum	Strata1	1	PELBOT	Station	CH0121/18	0.5	true
Stratum	Strata1	1	PELBOT	Station	RE0122/9	0.5	true
Stratum	Strata2	1	PELBOT	Station	CH0121/10	0.5	true
Stratum	Strata2	1	PELBOT	Station	CH0121/17	0.5	true
Stratum	Strata2	1	PELBOT	Station	CH0121/18	0.5	true
Stratum	Strata2	1	PELBOT	Station	RE0122/6	0.5	true

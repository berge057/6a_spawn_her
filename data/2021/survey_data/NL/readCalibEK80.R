calibOut <- function(filePath,fileName,dataSetName){
  
  data <- xmlParse(file.path(filePath,fileName), useInternalNodes = TRUE)
  xml_data <- xmlToList(data)
  
  
  Firmware <- xml_data$Calibration$Common$Transceiver$SoftwareVersion
  Software <- xml_data$Calibration$Common$Application$SoftwareVersion
  frequency <- xml_data$Calibration$TargetHits$Frequency
  sphere <- as.data.frame(cbind(as.numeric(unlist(strsplit(xml_data$Calibration$TargetReference$Frequency,";"))),
                                as.numeric(unlist(strsplit(xml_data$Calibration$TargetReference$Response,";")))))
  targetno <- which(abs(sphere$V1-as.numeric(frequency)) == min(abs(sphere$V1-as.numeric(frequency))) )
  target <- sphere$V2[targetno]
  fdate <- as.character(xml_data$Calibration$Common$TimeOfFileCreation)
  fdate <-unlist(strsplit(fdate,"\\+"))[1]
  fdate <- gsub("T", " ",fdate)
  mdate <- format(as.POSIXct(fdate), "%Y-%B-%d")
  mtime <- format(as.POSIXct(fdate), "%T")
  
  nhit <- length(xml_data$Calibration$TargetHits)-1
  
  # store overall results
  res <- list(Firmware=Firmware,
              Software=Software,
              frequency=as.numeric(frequency),
              SphereType=xml_data$Calibration$TargetReference$Name,
              pulse=as.numeric(xml_data$Calibration$Common$TransceiverSetting$PulseLength)*1000,
              gain=xml_data$Calibration$CalibrationResults$Gain,
              TSrms=xml_data$Calibration$CalibrationResults$TsRmsError,
              sacorr=xml_data$Calibration$CalibrationResults$SaCorrection,
              BeamWidthAlongship=xml_data$Calibration$CalibrationResults$BeamWidthAlongship,
              BeamWidthAthwartship=xml_data$Calibration$CalibrationResults$BeamWidthAthwartship,
              dataSet=dataSetName,
              dateFull=fdate,
              date=mdate,
              time=mtime,
              nhit=nhit)
  
  hits<- ldply(xml_data$Calibration$TargetHits, data.frame)
  hits<- hits[c(2:length(hits$TsComp)),]
  hits$TsComp<-as.numeric(as.character(hits$TsComp))
  hits$TsUncomp<-as.numeric(as.character(hits$TsUncomp))
  hits$TsUncomp_linear<-10^(hits$TsUncomp/10)
  hits$TsComp_linear<-10^(hits$TsComp/10)
  hits$Along <- as.numeric(as.character(hits$Along))
  hits$Athwart <- as.numeric(as.character(hits$Athwart))
  # filtering of hits to those within 3 degrees
  cent_dist <- 3
  #hits_filtered <-hits[((hits$Along)^2 + abs(hits$Athwart)^2)^0.5 < cent_dist,]
  hits <- hits[((hits$Along)^2 + abs(hits$Athwart)^2)^0.5 < cent_dist,]
  #hits_filtered <- hits
  
  Gf_ori <- as.numeric(xml_data$Calibration$Common$PreviousModelParameters$Gain)
  TS_comp <- 10*log10(hits$TsComp_linear)
  TSthCalib <- target
  
  hits$gain_all <- (TS_comp-TSthCalib)/2+Gf_ori
  
  nhit <- dim(hits)[1]
  
  hits$TS_SQdiff <- (hits$TsComp-target)^2
  hits$circle1 <- Im((exp(2*pi*seq(-2,2,length=nhit)*1i)))
  hits$clen1 <- Re((exp(2*pi*seq(-2,2,length=nhit)*1i)))
  
  hits$circle3 <- Im((exp(2*pi*seq(-2,2,length=nhit)*1i))*3.5)
  hits$clen3 <- Re((exp(2*pi*seq(-2,2,length=nhit)*1i))*3.5)
  
  hits$circle5.5 <- Im((exp(2*pi*seq(-2,2,length=nhit)*1i))*5.5)
  hits$clen5.5 <- Re((exp(2*pi*seq(-2,2,length=nhit)*1i))*5.5)
  
  hits <- cbind(hits,
                rep(dataSetName,dim(hits)[1]),
                rep(res$SphereType,dim(hits)[1]),
                rep(as.numeric(res$frequency),dim(hits)[1]),
                rep(mdate,dim(hits)[1]))
  
  colnames(hits)[(dim(hits)[2]-3):dim(hits)[2]] <- c('data_set','sphere_type','frequency','date')
  
  return(list(res=res,hits=hits))
}
compute_6aSPAWN_strata_mat <- function( endTab,
                                        ageVec,
                                        surveyYear,
                                        strataAll,
                                        spawn_mode){
  
  endTab$age <- as.numeric(endTab$age)
  nAges   <- length(ageVec)
  minAge  <- min(ageVec)
  maxAge  <- max(ageVec)
  
  # initialize array nStrata x nAges x maturity stages
  index6aSPAWN <- array(0, 
                        dim=c(length(strataAll),nAges,4,3),
                        dimnames=list(ac(strataAll),
                                      ac(minAge:maxAge),
                                      c('all','MAT','IMM','SPAWN'),c('abu','biomass','length')))
  


  uniqueStrata <- unique(endTab$strata)

  for(idxStrata in uniqueStrata){
    endTabFilt  <- endTab[endTab$strata == idxStrata,]

    # loop on all the index ages (1-9+) with 9 as plus group
    for(idxAges in ageVec){

      # select age to be computed, make sure we combine ages for the plus group
      if(idxAges == maxAge){
        # select ages as plut grounp
        ageSel <- unique(endTabFilt$age)[unique(endTabFilt$age) >= idxAges]
      }else{
        # select current age
        ageSel <- idxAges
      }
      
      # all stages
      endTabFiltAge  <- endTabFilt[ endTabFilt$age %in% ageSel,]

      
      index6aSPAWN[ac(idxStrata),ac(idxAges),'all','abu'] <- sum(endTabFiltAge$Abundance)
      index6aSPAWN[ac(idxStrata),ac(idxAges),'all','biomass'] <- sum(endTabFiltAge$biomass_g)
      index6aSPAWN[ac(idxStrata),ac(idxAges),'all','length'] <- sum(endTabFiltAge$length*endTabFiltAge$Abundance)
      
      # IMM stages
      endTabFiltAge_IMM  <- endTabFilt[ endTabFilt$age %in% ageSel &
                                        endTabFilt$maturity == 'IMM',]

      index6aSPAWN[ac(idxStrata),ac(idxAges),'IMM','abu'] <- sum(endTabFiltAge_IMM$Abundance)
      index6aSPAWN[ac(idxStrata),ac(idxAges),'IMM','biomass'] <- sum(endTabFiltAge_IMM$biomass_g)
      index6aSPAWN[ac(idxStrata),ac(idxAges),'IMM','length'] <- sum(endTabFiltAge_IMM$length*endTabFiltAge_IMM$Abundance)
      
      # MAT stages
      endTabFiltAge_MAT  <- endTabFilt[ endTabFilt$age %in% ageSel &
                                        endTabFilt$maturity == 'MAT',]
      
      index6aSPAWN[ac(idxStrata),ac(idxAges),'MAT','abu'] <- sum(endTabFiltAge_MAT$Abundance)
      index6aSPAWN[ac(idxStrata),ac(idxAges),'MAT','biomass'] <- sum(endTabFiltAge_MAT$biomass_g)
      index6aSPAWN[ac(idxStrata),ac(idxAges),'MAT','length'] <- sum(endTabFiltAge_MAT$length*endTabFiltAge_MAT$Abundance)
      
      # spawning stage
      if(spawn_mode == 1){
      endTabFiltAge_SPAWN  <- endTabFilt[ endTabFilt$age %in% ageSel &
                                          endTabFilt$stock == 'her-vian' & 
                                          endTabFilt$maturity == 'MAT',]
      }else if(spawn_mode == 2){
        endTabFiltAge_SPAWN  <- endTabFilt[ endTabFilt$age %in% ageSel &
                                            (endTabFilt$maturity_scale == 3 | endTabFilt$maturity_scale == 4),]
      }
      
      index6aSPAWN[ac(idxStrata),ac(idxAges),'SPAWN','abu'] <- sum(endTabFiltAge_SPAWN$Abundance)
      index6aSPAWN[ac(idxStrata),ac(idxAges),'SPAWN','biomass'] <- sum(endTabFiltAge_SPAWN$biomass_g)
      index6aSPAWN[ac(idxStrata),ac(idxAges),'SPAWN','length'] <- sum(endTabFiltAge_SPAWN$length*endTabFiltAge_SPAWN$Abundance)
    }
  }
  
  # weight at age vector
  waMat <- as.data.frame(array(0, dim=c(nAges,2)))
  colnames(waMat) <- c('age','VIaSPAWN')
  waMat$age <- ageVec
  
  waMat[,'VIaSPAWN'] <- apply(index6aSPAWN[,,'all','biomass'],2,sum)/
                        apply(index6aSPAWN[,,'all','abu'],2,sum)

  #sum(apply(indexHERASComponent.NSAS[,,'all','abu'],2,sum)*waMat[,'NSAS'])
  #sum(apply(indexHERASComponent.NSAS[,,'all','biomass'],2,sum))
  
  # length at age vector
  laMat <- as.data.frame(array(0, dim=c(nAges,2)))
  colnames(laMat) <- c('age','VIaSPAWN')
  laMat$age <- ageVec
  
  laMat[,'VIaSPAWN'] <- apply(index6aSPAWN[,,'all','length'],2,sum)/
                        apply(index6aSPAWN[,,'all','abu'],2,sum)

  # proportion mature vector
  propMat <- as.data.frame(array(0, dim=c(nAges,2)))
  colnames(propMat) <- c('age','VIaSPAWN')
  propMat$age <- ageVec
  
  propMat[,'VIaSPAWN'] <-  apply(index6aSPAWN[,,'MAT','abu'],2,sum)/
                          apply(index6aSPAWN[,,'all','abu'],2,sum)

  # get rid of the biomass dimension in the index matrices
  indexHERASComponent.NSAS <- drop(index6aSPAWN[,,,c('abu','biomass')])
  indexHERASComponent.WBSS <- drop(index6aSPAWN[,,,c('abu','biomass')])
  
  return(list(VIaSPAWN = index6aSPAWN,
              propM   = propMat,
              wa      = waMat,
              la      = laMat))
  
}